package com.ejemplo2.appconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ejemplo2.service.ClienteService;
import com.ejemplo2.service.ClienteServiceImpl;
import com.ejemplo2.service.DetalleFacturaService;
import com.ejemplo2.service.DetalleFacturaServiceImpl;
import com.ejemplo2.service.FacturaService;
import com.ejemplo2.service.FacturaServiceImpl;
import com.ejemplo2.service.ProductoService;
import com.ejemplo2.service.ProductoServiceImpl;

@Configuration
public class AppConfig {
	
	@Bean
	public ClienteService clienteService() {
		return new ClienteServiceImpl();
	}
		
	@Bean
	public ProductoService productoService() {
		return new ProductoServiceImpl();
	}
	
	
	@Bean
	public DetalleFacturaService detalleFacturaService() {
		return new DetalleFacturaServiceImpl();
	}
	
	@Bean
	public FacturaService facturaService() {
		return new FacturaServiceImpl();
	}

}
