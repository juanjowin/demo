package com.ejemplo2.dao;

import com.ejemplo2.model.ClienteModel;

public interface ClienteDao {
	
	public int save(ClienteModel clienteModel);
	
}
