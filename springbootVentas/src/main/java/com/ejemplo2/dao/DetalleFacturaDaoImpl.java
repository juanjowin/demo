package com.ejemplo2.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ejemplo2.model.DetalleFacturaModel;

@Repository
public class DetalleFacturaDaoImpl implements DetalleFacturaDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;


	@Override
	public void save(DetalleFacturaModel detalleFactura) {
		//jdbcTemplate.batchUpdate(sql)
		jdbcTemplate.update("insert into detalle_factura(idfactura,idproducto,cantidad,precio) values(?,?,?,?)",
				detalleFactura.getIdfactura(),detalleFactura.getIdproducto(),detalleFactura.getCantidad(),detalleFactura.getPrecio());
	}

	@Override
	public void update(DetalleFacturaModel detalleFactura) {
		jdbcTemplate.update("update detalle_factura set idfactura=?,idproducto=?,cantidad=?,precio=? where iddetalle=?",
				detalleFactura.getIdfactura(),detalleFactura.getIdproducto(),detalleFactura.getCantidad(),detalleFactura.getPrecio(),detalleFactura.getIddetalle());
	}

	@Override
	public DetalleFacturaModel findById(int id) {
		return jdbcTemplate.queryForObject("select * from detalle_factura where iddetalle=?", new DetalleFacturaModelRowMapper(), id); 
		        
	}

	@Override
	public void delete(int id) {
		 jdbcTemplate.update("delete from detalle_factura where iddetalle=?", id); 
		
	}

}
