package com.ejemplo2.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ejemplo2.model.DetalleFacturaModel;

public class DetalleFacturaModelRowMapper implements RowMapper<DetalleFacturaModel> {

	@Override
	public DetalleFacturaModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		DetalleFacturaModel detalleFacturaModel=new DetalleFacturaModel();
		detalleFacturaModel.setIddetalle(rs.getInt("iddetalle"));
		detalleFacturaModel.setIdfactura(rs.getInt("idfactura"));
		detalleFacturaModel.setIdproducto(rs.getInt("idproducto"));
		detalleFacturaModel.setCantidad(rs.getInt("cantidad"));
		detalleFacturaModel.setPrecio(rs.getDouble("precio"));
		
		return detalleFacturaModel;
	}

}
