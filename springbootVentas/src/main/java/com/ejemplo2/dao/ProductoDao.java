package com.ejemplo2.dao;

import com.ejemplo2.model.ProductoModel;

public interface ProductoDao {
	public int save(ProductoModel productoModel);
}
