package com.ejemplo2.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ejemplo2.model.ProductoModel;

@Repository
public class ProductoDaoImpl implements ProductoDao{
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public int save(ProductoModel productoModel) {
		
		return jdbcTemplate.update("insert into producto(nombreproducto,precio)values(?,?)",productoModel.getNombreproducto(),productoModel.getPrecio());
	}
	
}
