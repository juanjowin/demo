package com.ejemplo2.model;

public class DetalleFacturaModel {

	
	private int iddetalle;
	private int idfactura;
	private int idproducto;
	private int cantidad;
	private double precio;
	
	public int getIddetalle() {
		return iddetalle;
	}
	public void setIddetalle(int iddetalle) {
		this.iddetalle = iddetalle;
	}
	public int getIdfactura() {
		return idfactura;
	}
	public void setIdfactura(int idfactura) {
		this.idfactura = idfactura;
	}
	public int getIdproducto() {
		return idproducto;
	}
	public void setIdproducto(int idproducto) {
		this.idproducto = idproducto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	@Override
	public String toString() {
		return "DetalleFacturaModel [iddetalle=" + iddetalle + ", idfactura=" + idfactura + ", idproducto=" + idproducto
				+ ", cantidad=" + cantidad + ", precio=" + precio + "]";
	}
}
