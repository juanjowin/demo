package com.ejemplo2.model;


import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class FacturaModel {


	private int idfactura;
	private int idcliente;
	private String numfacura;
	private LocalDateTime fechacreacion;
	
	public int getIdfactura() {
		return idfactura;
	}
	public void setIdfactura(int idfactura) {
		this.idfactura = idfactura;
	}
	public int getIdcliente() {
		return idcliente;
	}
	public void setIdcliente(int idcliente) {
		this.idcliente = idcliente;
	}
	public String getNumfacura() {
		return numfacura;
	}
	public void setNumfacura(String numfacura) {
		this.numfacura = numfacura;
	}
	public LocalDateTime getFechacreacion() {
		return fechacreacion;
	}
	public void setFechacreacion(LocalDateTime fechacreacion) {
		this.fechacreacion = fechacreacion;
	}

	@Override
	public String toString() {
		return "FacturaModel [idfactura=" + idfactura + ", idcliente=" + idcliente + ", numfacura=" + numfacura
				+ ", fechacreacion=" + fechacreacion + "]";
	}
		
}
