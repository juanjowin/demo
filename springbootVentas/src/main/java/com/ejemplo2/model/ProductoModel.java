package com.ejemplo2.model;

public class ProductoModel {

	private int idproducto;
	private String nombreproducto;
	private double precio;
	
	@Override
	public String toString() {
		return "productoModel [idproducto=" + idproducto + ", nombreproducto=" + nombreproducto + ", precio=" + precio
				+ "]";
	}
	public int getIdproducto() {
		return idproducto;
	}
	public void setIdproducto(int idproducto) {
		this.idproducto = idproducto;
	}
	public String getNombreproducto() {
		return nombreproducto;
	}
	public void setNombreproducto(String nombreproducto) {
		this.nombreproducto = nombreproducto;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	
}
