package com.ejemplo2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ejemplo2.dao.ClienteDao;
import com.ejemplo2.dao.ClienteDaoImpl;
import com.ejemplo2.model.ClienteModel;

@Service
public class ClienteServiceImpl implements ClienteService{
	
	
	@Autowired
	ClienteDaoImpl clienteRepositoryImpl;
	
	@Override
	public void save(ClienteModel clienteModel) {
		clienteRepositoryImpl.save(clienteModel);
	}

}
