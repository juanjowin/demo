package com.ejemplo2.service;

import com.ejemplo2.model.DetalleFacturaModel;

public interface DetalleFacturaService {
	
	public void save(DetalleFacturaModel detalleFactura);
	public void update(DetalleFacturaModel detalleFactura);
	public void delete(int id);
	public DetalleFacturaModel findById(int id);
}
