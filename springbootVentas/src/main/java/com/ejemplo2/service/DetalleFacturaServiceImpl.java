package com.ejemplo2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ejemplo2.dao.DetalleFacturaDao;
import com.ejemplo2.model.DetalleFacturaModel;

@Service
public class DetalleFacturaServiceImpl implements DetalleFacturaService {

	@Autowired
	private DetalleFacturaDao detalleFacturaDao;
	
	@Override
	public void save(DetalleFacturaModel detalleFactura) {
		 detalleFacturaDao.save(detalleFactura);
	}

	@Override
	public void update(DetalleFacturaModel detalleFactura) {
		detalleFacturaDao.update(detalleFactura);
		
	}

	@Override
	public DetalleFacturaModel findById(int id) {
		return detalleFacturaDao.findById(id);
	}

	@Override
	public void delete(int id) {
		 detalleFacturaDao.delete(id);
		
	}

}
