package com.ejemplo2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.ejemplo2.dao.FacturaDao;
import com.ejemplo2.model.FacturaModel;

@Service
public class FacturaServiceImpl implements FacturaService{
	
	@Autowired
	private FacturaDao  facturaDao;

	@Override
	public void save(FacturaModel facturaModel) {
		facturaDao.save(facturaModel);
	}

	@Override
	public void update(FacturaModel facturaModel) {
		facturaDao.update(facturaModel);
	}

	@Override
	public void delete(int id) {
		facturaDao.delete(id);
		
	}

	@Override
	public FacturaModel findById(int id) {
		return facturaDao.findById(id);
	}
	
}
