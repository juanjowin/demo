package com.ejemplo2.service;

import com.ejemplo2.model.ProductoModel;

public interface ProductoService {

	public int save(ProductoModel productoModel);
}
