package com.ejemplo2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.ejemplo2.dao.ProductoDao;
import com.ejemplo2.model.ProductoModel;

@Service
public class ProductoServiceImpl implements ProductoService{

	@Autowired
	private ProductoDao productoDao;
	
	@Override
	public int save(ProductoModel productoModel) {
		return productoDao.save(productoModel);
	}

}
