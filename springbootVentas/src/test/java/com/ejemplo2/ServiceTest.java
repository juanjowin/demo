package com.ejemplo2;


import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.context.annotation.Import;
import com.ejemplo2.appconfig.AppConfig;
import com.ejemplo2.model.ClienteModel;
import com.ejemplo2.model.DetalleFacturaModel;
import com.ejemplo2.model.FacturaModel;
import com.ejemplo2.model.ProductoModel;
import com.ejemplo2.service.ClienteServiceImpl;
import com.ejemplo2.service.DetalleFacturaService;
import com.ejemplo2.service.FacturaService;
import com.ejemplo2.service.ProductoService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(AppConfig.class)
public class ServiceTest {

	@Autowired
	private ClienteServiceImpl clienteServiceImpl;
	@Autowired
	private ProductoService productoService;
	@Autowired
	private DetalleFacturaService detalleFacturaService;
	@Autowired
	private FacturaService facturaService;
	
	
		
		/*@Test
		public void InsertarCliente() {
		
			ClienteModel clienteModel=new ClienteModel();
			try {
				clienteModel.setNombre("monica marcela");
				clienteModel.setApellido("Ramirez Lopez");
				clienteModel.setTelefono("2216-8989");
				clienteModel.setEmail("moni@gmail.com");
				
				clienteServiceImpl.save(clienteModel);
				
				System.out.println("cliente insertado con exito");
				
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
		
		@Test
		public void insertarProducto() {
		
			ProductoModel productoModel = new ProductoModel();
			try {
				productoModel.setNombreproducto("mause");
				productoModel.setPrecio(5.50);
				
				productoService.save(productoModel);
				
				System.out.println("producto se inserto correctamente");
				
			} catch (Exception e) {
				e.getMessage();
			}
		
		}*/
		
		@Test
		public void insertarFactura() {
			FacturaModel facturaModel =new FacturaModel();
			
			try {
				LocalDateTime fechacreacion = LocalDateTime.now();
				
				facturaModel.setIdcliente(2);
				facturaModel.setNumfacura("000-002");
				facturaModel.setFechacreacion(fechacreacion);
				
				facturaService.save(facturaModel);
				
				System.out.println("factura se inserto con exito");
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
	
		}
		
		@Test
		public void modificarFactura() {
			LocalDateTime fechacreacion = LocalDateTime.now();
			
			try {
				
				FacturaModel facturaModel=facturaService.findById(3);
				System.out.println(facturaModel);
			
				facturaModel.setIdcliente(1);
				facturaModel.setNumfacura("000-003");
				facturaModel.setFechacreacion(fechacreacion);
				
				facturaService.update(facturaModel);
				
				System.out.println("se modifico con exito");
				
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
		
		@Test
		public void buscaFacturaPorId() {
			FacturaModel facturaModel=facturaService.findById(1);
			System.out.println(facturaModel);
		}
		
		@Test
		public void eliminarFactura() {
			FacturaModel facturaModel=facturaService.findById(2);
			System.out.println(facturaModel);
		
			facturaService.delete(facturaModel.getIdfactura());
			System.out.println("Registro se elimino con exito");
		}
		
		@Test
		public void insertarDetalleFactura() {
		
			ArrayList<DetalleFacturaModel> lista=new ArrayList<DetalleFacturaModel>();
			 DetalleFacturaModel detalleFactura=new DetalleFacturaModel();
		 
			try {					
				detalleFactura.setIdfactura(1);
				detalleFactura.setIdproducto(2);
				detalleFactura.setCantidad(10);
				detalleFactura.setPrecio(5.50);
			
				lista.add(detalleFactura);
				
				detalleFacturaService.save(detalleFactura);
							
				System.out.println("se inserto con exito");
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		@Test
		public void buscarPorId() {
			try {
				DetalleFacturaModel detalleFacturaModel=detalleFacturaService.findById(6);
				System.out.println(detalleFacturaModel);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		
		@Test
		public void modificarDetalleFactura() {
			
			try {
				DetalleFacturaModel detalleFactura=detalleFacturaService.findById(6);
				System.out.println(detalleFactura);
			
				detalleFactura.setIdfactura(1);
				detalleFactura.setIdproducto(1);
				detalleFactura.setCantidad(4);
				detalleFactura.setPrecio(3.00);	
				
				detalleFacturaService.update(detalleFactura);
				
				System.out.println("se modifico con exito");
				
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
		}
		
		@Test
		public void eliminarDetalleFactura() {
			try {
				DetalleFacturaModel detalleFactura=detalleFacturaService.findById(9);
				System.out.println(detalleFactura);
			
				detalleFacturaService.delete(detalleFactura.getIddetalle());
				System.out.println("Registro se elimino con exito");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
}

